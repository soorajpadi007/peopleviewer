//
//  PeopleViewerTests.swift
//  PeopleViewerTests
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import XCTest

final class PeopleViewerTests: XCTestCase {
    func testLoadDataSuccess() {
        let viewModel = ContentViewModel()
        viewModel.loadData()
        XCTAssertTrue(!viewModel.persons.isEmpty)
    }
    
    func testLoadDataFileNotFound() {
        if Bundle.main.url(forResource: "data", withExtension: "json") != nil {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false, "Data file not found.")
        }
    }
    
    func testCreateNewPerson() {
        // Create a new person
        let newPerson = Person(name: "Abc", info: Detail(title: "Mr", age: 30, phone: "1234567890", email: "abc@example.com"))
        
        // Assert that the properties of the new person are correctly set
        XCTAssertEqual(newPerson.name, "Abc", "Name should be 'Abc'")
        XCTAssertEqual(newPerson.info.title, "Mr", "Title should be 'Mr'")
        XCTAssertEqual(newPerson.info.age, 30, "Age should be 30")
        XCTAssertEqual(newPerson.info.phone, "1234567890", "Phone number should be '1234567890'")
        XCTAssertEqual(newPerson.info.email, "abc@example.com", "Email should be 'john@example.com'")
    }
}
