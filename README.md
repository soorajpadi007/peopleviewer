# PeopleViewer - SwiftUI Technical Assessment

## Instructions

1. **Running the App**:
   - Open the `PeopleViewer` project in Xcode.
   - Make sure you have Xcode installed and updated.
   - Compile and run the project on your desired simulator or device.

2. **Functionality**:
   - Upon running the app, you'll see a sidebar containing a list of people's names.
   - Clicking on a name will display the person's details on the right side.
   - You can also create a new person by clicking the "Create Person" button.

3. **Unit Testing**:
   - The project includes unit tests to ensure functionality.

4. **Additional Notes**:
   - Ensure the `data.json` file is included in the project resources folder for data import.

## Project Structure

- **PeopleViewerApp.swift**: Entry point of the application.
- **Person.swift**: Model structure representing a person.
- **Detail.swift**: Model structure representing details of a person.
- **ContentView.swift**: Main view containing sidebar, detail view, and person creation.
- **CreateNewPersonView.swift**: View for creating a new person.
- **DetailView.swift**: View for displaying details of a selected person.
- **SidebarView.swift**: Sidebar view displaying a list of persons.
- **ContentViewModel.swift**: ViewModel for managing data and business logic.

