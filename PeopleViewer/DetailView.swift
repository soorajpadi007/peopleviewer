//
//  DetailView.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import SwiftUI

struct DetailView: View {
    let selectedPerson: Person
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text(selectedPerson.info.title + " " + selectedPerson.name)
            Text("Age: \(selectedPerson.info.age)")
            HStack {
                Image(systemName: "phone")
                Text(selectedPerson.info.phone)
            }
            HStack {
                Image(systemName: "mail")
                Text(selectedPerson.info.email)
            }
        }
        .padding()
        .cornerRadius(20)
        .overlay(
            RoundedRectangle(cornerRadius: 20)
                .stroke(.black, lineWidth: 1)
        )
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
        .navigationTitle(selectedPerson.name)
    }
}

#Preview {
    DetailView(selectedPerson: Person(
        name: "Abc",
        info: Detail(
            title: "Mr",
            age: 25,
            phone: "9876543210",
            email:" a@abc.com")))
}
