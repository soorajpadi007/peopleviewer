//
//  ContentViewModel.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import Foundation
import os.log

class ContentViewModel: ObservableObject {
    @Published var persons: [Person] = []
    private let logger: Logger
    
    init() {
        self.logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: String(describing: ContentViewModel.self))
        loadData() // Loading people data items from data.json
    }
    
    // Method to load data from data.json
    func loadData() {
        guard let url = Bundle.main.url(forResource: "data", withExtension: "json") else {
            logger.log("Unable to find data.json file")
            return
        }
        do {
            let data = try Data(contentsOf: url)
            let decodedData = try JSONDecoder().decode([Person].self, from: data)
            persons = decodedData
        } catch {
            logger.error("Unable to parse json objects \(error.localizedDescription, privacy: .public)")
        }
    }
}
