//
//  CreateNewPersonView.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 06/03/24.
//

import SwiftUI
import Combine

struct CreateNewPersonView: View {
    @Environment(\.dismiss) var dismiss
    @State private var name: String = ""
    @State private var age: String = ""
    @State private var email: String = ""
    @State private var title: String = ""
    @State private var phone: String = ""
    @State private var showAlert = false
    let completion: (Person?) -> Void
    
    var body: some View {
        #if os(iOS)
        NavigationView {
            contentView
                .navigationBarTitle("Create Person", displayMode: .inline)
                .toolbar {
                    ToolbarItem(placement: .confirmationAction) {
                        creatButtonView
                    }
                    ToolbarItem(placement: .cancellationAction) {
                        cancelButtonView
                    }
                }
        }
        #else
        VStack {
            Text("Create Person")
                .fontWeight(.semibold)
            contentView
                .padding(.top, 10)
            HStack {
                cancelButtonView
                Spacer()
                creatButtonView
            }
            .padding(.top, 10)
        }
        .padding()
        .frame(width: 250)
        #endif
    }
    
    @ViewBuilder private var contentView: some View {
        Form {
            TextField("Name", text: $name)
            TextField("Title", text: $title)
            TextField("Age", text: $age)
                .onReceive(Just(age)) { newValue in
                    let filtered = newValue.filter { "0123456789".contains($0) }
                    if filtered != newValue {
                        self.age = filtered
                    }
                }
            TextField("Email", text: $email)
            TextField("Phone", text: $phone)
        }
        .alert("Validation Error", isPresented: $showAlert, actions: { }, message: {
            Text("Name and email cannot be blank.")
        })
        .autocorrectionDisabled()
    }
    
    @ViewBuilder private var creatButtonView: some View {
        Button("Create") {
            if validateInput() {
                let info = Detail(title: title, age: Int(age) ?? 0, phone: phone, email: email)
                let person = Person(name: name, info: info)
                completion(person)
                dismiss()
            } else {
                showAlert = true
            }
        }
    }
    
    @ViewBuilder private var cancelButtonView: some View {
        Button("Cancel") {
            dismiss()
        }
    }
    
    private func validateInput() -> Bool {
        return !name.isEmpty && !email.isEmpty
    }
}

#Preview {
    CreateNewPersonView { _ in }
}
