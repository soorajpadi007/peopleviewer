//
//  ContentView.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var viewModel = ContentViewModel()
    @State private var selectedPerson: Person?
    @State private var showPersonCreateSheet: Bool = false
    
    var body: some View {
        NavigationSplitView {
            // SidebarView displaying list of persons
            SidebarView(persons: viewModel.persons, selectedPerson: $selectedPerson)
                #if os(iOS)
                .toolbar {
                    Button("Create Person") {
                        showPersonCreateSheet = true
                    }
                }
                #endif
        } detail: {
            // DetailView displaying details of selected person
            if let selectedPerson = selectedPerson {
                DetailView(selectedPerson: selectedPerson)
            } else {
                Text("Select a person from the sidebar")
            }
        }
        .sheet(isPresented: $showPersonCreateSheet, content: {
            // CreateNewPersonView for creating a new person
            CreateNewPersonView { person in
                if let person {
                    viewModel.persons.append(person)
                }
            }
        })
        #if !os(iOS)
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button("Create Person") {
                    showPersonCreateSheet = true
                }
            }
        }
        #endif
    }
}

#Preview {
    ContentView()
}
