//
//  PeopleViewerApp.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import SwiftUI

@main
struct PeopleViewerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
