//
//  SidebarView.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import SwiftUI

struct SidebarView: View {
    let persons: [Person]
    @Binding var selectedPerson: Person?
    
    var body: some View {
        List(selection: $selectedPerson) {
            ForEach(persons, id: \.self) { person in
                Text(person.name)
            }
        }
        #if os(macOS)
        .frame(minWidth: 200)
        #else
        .navigationTitle("Persons")
        #endif
    }
}

#Preview {
    SidebarView(persons: [
        Person(name: "Mr A", info: Detail(title: "Test Job", age: 25, phone: "9876543210", email:" a@abc.com"))
    ], selectedPerson: .constant(nil))
}
