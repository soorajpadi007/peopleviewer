//
//  Detail.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import Foundation

// Model structure representing details of a person
struct Detail: Codable, Hashable {
    let title: String
    let age: Int
    let phone: String
    let email: String
}
