//
//  Person.swift
//  PeopleViewer
//
//  Created by Sooraj Padiillam on 05/03/24.
//

import Foundation

// Model structure representing a person
struct Person: Codable, Hashable {
    let name: String
    let info: Detail
    
    // Custom hash method to ensure correct hashing for hashing-based operations
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(info)
    }
    
    // Custom equality check based on name and info properties
    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.name == rhs.name && lhs.info == rhs.info
    }
}
